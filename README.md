# Nushell configuration


## Installation

```bash
wget https://github.com/nushell/nushell/releases/download/0.91.0/nu-0.91.0-x86_64-unknown-linux-musl.tar.gz
tar -xzf nu-0.91.0-x86_64-unknown-linux-musl.tar.gz 
mv nu-0.91.0-x86_64-unknown-linux-musl/nu /usr/local/bin/nu
```

### Installation eza

> https://github.com/eza-community/eza/releases/tag/v0.18.9
