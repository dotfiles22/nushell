
export def main [
  program: string
] {
  $env.GUM_FORMAT_THEME = ("~/.config/glow/catppuccin.json" | path expand) 
  tldr $program --raw | gum format | gum pager
  man $program | bat -l man --theme=bat --pager="gum pager --show-line-numbers=false"
}
