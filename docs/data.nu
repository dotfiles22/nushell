# Types of data

## Integer

let integer_example_1 = 12
let integer_example_2 = -12
let integer_example_3 = 012012
let integer_example_4 = 120394

print '=== Integer ==='
print $integer_example_1 $integer_example_2 $integer_example_3 $integer_example_4
print ($integer_example_1 | describe)
## Decimals

### Decimals are floats

let decimal_example_1 = 12.2
let decimal_example_2 = -12.2
let decimal_example_3 = Infinity
let decimal_example_4 = 120394


print '=== Decimals (float) ==='
print $decimal_example_1 $decimal_example_2 $decimal_example_3 $decimal_example_4
print ($decimal_example_1 | describe)

## Strings

let string_example_1 = "Hello"
let string_example_2 = "World"
let string_example_3 = $"($string_example_1) ($string_example_2)"
# In the docs but does not work
# let string_example_4 = `foo bar`
# let string_example_5 = baz

print '=== Strings ==='
print $string_example_1 $string_example_2 $string_example_3 
print ($string_example_1 | describe)

# Booleans 

let boolean_1 = true
let boolean_2 = false

print '=== boolean ==='
print $boolean_1 $boolean_2 
print ($boolean_1 | describe)

# date

let date_1 = 2000-01-01
# Incorrect
# let date_2 = 1996-23-09
# Correct
let date_2 = 1996-09-23
let date_3 = $date_2 + 10wk
let date_4 = date now

print '=== date ==='
print $date_1 $date_2 $date_3 $date_4
print ($date_1 | describe)

# duration

let duration_1 = 2ns # Nanosecond
let duration_2 = 2us + 10us # Microsecond
let duration_3 = 15ms # Millisecond
let duration_4 = 1sec # Second
let duration_5 = 15min # Minutes
let duration_6 = 13hr # Hours
let duration_7 = 1day # Day
let duration_8 = 15wk # Week

print '=== duration ==='
print $duration_1 $duration_2 $duration_3 $duration_4  $duration_5  $duration_6  $duration_7 $duration_8
print ($duration_1 | describe)

# ------------------ #

# xyztemplate

# let xyztemplate_1 = 
# let xyztemplate_2 = 
# let xyztemplate_3 = 
# let xyztemplate_4 = 

# print '=== xyztemplate ==='
# print $xyztemplate_1 $xyztemplate_2 $xyztemplate_3 $xyztemplate_4
# print ($xyztemplate_1 | describe)


