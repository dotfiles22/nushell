use std *


$env.THEME_PRIMARY = "#f38ba8"

#$env.FZF_DEFAULT_OPTS = "--color=bg+:#313244,bg:#1e1e2e,spinner:#f5e0dc,hl:#f38ba8 --color=fg:#cdd6f4,header:#f38ba8,info:#cba6f7,pointer:#f5e0dc --color=marker:#f5e0dc,fg+:#cdd6f4,prompt:#cba6f7,hl+:#f38ba8"

$env.GUM_CHOOSE_CURSOR_FOREGROUND = $env.THEME_PRIMARY
$env.GUM_CHOOSE_HEADER_FOREGROUND = $env.THEME_PRIMARY

$env.EDITOR = 'nvim'
$env.VISUAL = 'bat'

path add ~/.local/oxide/bin
path add ~/.local/bin
path add ~/.cargo/bin

use ("~/.local/oxide/lib/nushell/session" | path expand)
use ("~/.local/oxide/lib/nushell/notification" | path expand) 
use ("~/.local/oxide/lib/nushell/database" | path expand) 
use ("~/.local/oxide/lib/nushell/vcs" | path expand) 
use ("~/.local/oxide/lib/nushell/task" | path expand) 

alias ls-data = ls
alias ls = eza --icons

alias pager = gum pager

alias user-input-select = gum choose
alias user-input-file = gum file
alias user-input-directoy = gum file --directory

# TODO: See if there is a way to check if php exists before aliasing
alias xphp = php -dxdebug.mode=off

export def procs [] {
  if ($env | get -i ZELLIJ | describe) != 'nothing' {
    zellij action rename-tab "Process"
  }
  mprocs
}

export def connect [] {
  let servers = (open ("~/.local/servers.json" | path expand) | get servers)
  let to_connect = (gum choose ...($servers | columns))
  let config = $servers | get $to_connect
  gum format $"Connecting to **($config.user)**@**($config.host)**"
  sshpass -p $"($config.pass)" ssh $"($config.user)@($config.host)"
}


alias progress = gum spin --show-output --align='right' --spinner='dot'
alias edit = nvim
source ~/.config/nushell/local.nu 
